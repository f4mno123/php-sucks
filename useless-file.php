<?php
    $conn = mysqli_connect("localhost", "root", "", "uselessdb");
    if ($conn){
        // fetch data from tables 'answers' and 'questions'
        $sql = "SELECT * FROM answers, questions WHERE answers.question_id = questions.id";
        $result = mysqli_query($conn, $sql);
        if (mysqli_num_rows($result) > 0){
            while ($row = mysqli_fetch_assoc($result)){
                echo $row['question'] . "<br>";
                echo $row['answer'] . "<br>";
            }
        } else {
            echo "No data found";
        }

    } else {
        echo "Failed to connect to database";
    }
